package com.juick.listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juick.listener.objects.JuickMessage;
import com.juick.listener.ws.WsClient;
import com.juick.listener.ws.WsClientListener;

public class WSFeedAdapter extends BaseAdapter {
	private List<JuickMessage> mPosts;
	private Context mContext;

	public WSFeedAdapter(Context context, final String wsPath) {
		mPosts = new ArrayList<JuickMessage>();
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		JuickMessage msg = getItem(position);
		View v = convertView;
		if (v == null || !(v instanceof RelativeLayout)) {
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.post, null);

			ViewHolder holder = new ViewHolder();
			holder.nick = (TextView) v.findViewById(R.id.txtNick);
			holder.date = (TextView) v.findViewById(R.id.txtDate);
			holder.body = (TextView) v.findViewById(R.id.txtBody);
			holder.mid = (TextView) v.findViewById(R.id.txtId);
			v.setTag(holder);
		}

		ViewHolder vh = (ViewHolder) v.getTag();

		vh.nick.setText("@" + msg.User.UName);
		vh.date.setText(msg.Timestamp.toString());
		vh.body.setText(msg.Text);
		vh.mid.setText(Integer.toString(msg.MID));
		return v;
	}

	private class ViewHolder {
		public TextView mid;
		public TextView body;
		public TextView date;
		public TextView nick;
	}

	private void clear() {
		clear(false);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mPosts.size();
	}

	public JuickMessage getItem(int position) {
		// TODO Auto-generated method stub
		return mPosts.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private void clear(boolean needUpdate) {
		mPosts.clear();
		if (needUpdate)
			notifyDataSetChanged();
	}

	public void add(JuickMessage post) {
		add(post, false);
	}
	
	public void add(JuickMessage post, boolean update){
		if(mPosts.size() > 30)
			mPosts = mPosts.subList(30, mPosts.size());
		mPosts.add(0, post);
		if(update)
			notifyDataSetChanged();
	}
}
