package com.juick.listener;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juick.listener.objects.JuickMessage;

public class CommentsListenerFragment extends ListFragment {
	private WSFeedAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.comments, null);
		mAdapter = new WSFeedAdapter(view.getContext(), "/replies");
		setListAdapter(mAdapter);
		return view;
	}
	
	public void add(JuickMessage msg){
		mAdapter.add(msg, true);
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}
}
