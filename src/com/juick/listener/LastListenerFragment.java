package com.juick.listener;

import com.juick.listener.objects.JuickMessage;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class LastListenerFragment extends ListFragment {
	protected static final int NEW_COMMENT = 0;

	private WSFeedAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.last, null);
		mAdapter = new WSFeedAdapter(view.getContext(), "/all");
		//ListView lastList = (ListView) view.findViewById(R.id.lastList);
		//lastList.setAdapter(mAdapter);
		setListAdapter(mAdapter);
		return view;
	}
	
	public void add(JuickMessage msg){
		mAdapter.add(msg, true);
	}
	
	
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}
}
