package com.juick.listener;

import java.lang.ref.WeakReference;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;

import com.juick.listener.objects.JuickMessage;
import com.juick.listener.services.WSService;

public class MainActivity extends FragmentActivity{

	protected Messenger mService;
	private Messenger mMessenger = new Messenger(new IncomingHandler(this));
	protected boolean mBound = false;
	private LastListenerFragment lastPostsFragment;
	private CommentsListenerFragment lastCommentsFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		
		

		FragmentManager fragmentManager = getSupportFragmentManager();
		
		lastPostsFragment = (LastListenerFragment)fragmentManager.findFragmentById(R.id.lastPostsFragment);
		lastCommentsFragment = (CommentsListenerFragment)fragmentManager.findFragmentById(R.id.lastCommentsFragment);
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(!mBound){
			Intent intent = new Intent(this, WSService.class);
			bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(mBound){
			try {
				Message disconnectMsg = Message.obtain(null, WSService.MSG_UNREGISTER_CLIENT);
				disconnectMsg.replyTo = mMessenger;
				mService.send(disconnectMsg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			unbindService(mConnection);
			mBound = false;
		}
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
			mBound = false;
			Log.i("MainActivity", "WSService disconnected");
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = new Messenger(service);
			 try {
                 Message msg = Message.obtain(null,
                         WSService.MSG_REGISTER_CLIENT);
                 msg.replyTo = mMessenger;
                 mService.send(msg);
                 Log.i("MainActivity", "WSService connected");
			 } catch (RemoteException e) {
				 Log.e("MainActivity", "WSService not started");
			}
			mBound  = true;
		}
	};
	
	
	private static class IncomingHandler extends Handler {
		private WeakReference<MainActivity> mActivity;
		
		public IncomingHandler(MainActivity activity) {
			mActivity = new WeakReference<MainActivity>(activity);
		}
		
		@Override
		public void handleMessage(Message msg) {
			if(mActivity.get() == null){
				Log.w("MainActivity", "Activity not found");
				return;
			}
			switch (msg.what) {
			case WSService.NEW_POST_OBTAINED:
				JuickMessage newPost = (JuickMessage)((Bundle)msg.obj).getParcelable("message");
				Log.i("MainActivity", "New post " + newPost.MID);
				mActivity.get().lastPostsFragment.add(newPost);
				break;
			case WSService.NEW_COMMENT_OBTAINED:
				JuickMessage newComment = (JuickMessage)((Bundle)msg.obj).getParcelable("message");
				Log.i("MainActivity", "New comment " + newComment.MID + "/" + newComment.RID);
				mActivity.get().lastCommentsFragment.add(newComment);
				break;
			}
			super.handleMessage(msg);
		}
	}

}
