package com.juick.listener.objects;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.juick.listener.ParcelableDate;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 *
 * @author ugnich
 */
public class JuickMessage implements Parcelable {

    public int MID = 0;
    public int RID = 0;
    public String Text = null;
    public JuickUser User = null;
    public List<String> tags = new ArrayList<String>();
    public ParcelableDate Timestamp = null;
    public int replies = 0;
    public String Photo = null;
    public String Video = null;

    public JuickMessage(){}
    
    public static JuickMessage parseJSON(JSONObject json) throws JSONException {
        JuickMessage jmsg = new JuickMessage();
        jmsg.MID = json.getInt("mid");
        if (json.has("rid")) {
            jmsg.RID = json.getInt("rid");
        }
        jmsg.Text = json.getString("body").replace("&quot;", "\"");
        jmsg.User = JuickUser.parseJSON(json.getJSONObject("user"));

        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            jmsg.Timestamp = new ParcelableDate(df.parse(json.getString("timestamp")));
        } catch (ParseException e) {
        }

        if (json.has("tags")) {
            JSONArray tags = json.getJSONArray("tags");
            for (int n = 0; n < tags.length(); n++) {
                jmsg.tags.add(tags.getString(n).replace("&quot;", "\""));
            }
        }

        if (json.has("replies")) {
            jmsg.replies = json.getInt("replies");
        }

        if (json.has("photo")) {
            jmsg.Photo = json.getJSONObject("photo").getString("small");
        }
        if (json.has("video")) {
            jmsg.Video = json.getJSONObject("video").getString("mp4");
        }

        return jmsg;
    }

    public String getTags() {
        String t = new String();
        for (String tag: tags) {
            if (t.length() > 0) {
                t += ' ';
            }
            t += '*' + tag;
        }
        return t;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof JuickMessage)) {
            return false;
        }
        JuickMessage jmsg = (JuickMessage) obj;
        return (this.MID == jmsg.MID && this.RID == jmsg.RID);
    }

    public int compareTo(Object obj) throws ClassCastException {
        if (!(obj instanceof JuickMessage)) {
            throw new ClassCastException();
        }
        JuickMessage jmsg = (JuickMessage) obj;

        if (this.MID != jmsg.MID) {
            if (this.MID > jmsg.MID) {
                return -1;
            } else {
                return 1;
            }
        }

        if (this.RID != jmsg.RID) {
            if (this.RID < jmsg.RID) {
                return -1;
            } else {
                return 1;
            }
        }

        return 0;
    }

    @Override
    public String toString() {
        String msg = "";
        if (User != null) {
            msg += "@" + User.UName + ": ";
        }
        msg += getTags();
        if (msg.length() > 0) {
            msg += "\n";
        }
        if (Photo != null) {
            msg += Photo + "\n";
        } else if (Video != null) {
            msg += Video + "\n";
        }
        if (Text != null) {
            msg += Text + "\n";
        }
        msg += "#" + MID;
        if (RID > 0) {
            msg += "/" + RID;
        }
        msg += " http://juick.com/" + MID;
        if (RID > 0) {
            msg += "#" + RID;
        }
        return msg;
    }

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		Log.d("JuickMessage", "Write to parcel");
		dest.writeInt(MID);
		dest.writeInt(RID);
		dest.writeString(Text);
		dest.writeString(Photo);
		dest.writeParcelable(User, 0);
		dest.writeList(tags);
		dest.writeParcelable(Timestamp, 0);
		dest.writeInt(replies);
		dest.writeString(Video);
	}
	
	public static final Parcelable.Creator<JuickMessage> CREATOR = new Parcelable.Creator<JuickMessage>() {
		public JuickMessage createFromParcel(Parcel in) {
			return new JuickMessage(in);
		}

		public JuickMessage[] newArray(int size) {
			return new JuickMessage[size];
		}
	};

	
	
	public JuickMessage(Parcel in){
		MID = in.readInt();
		RID = in.readInt();
		Text = in.readString();
		Photo = in.readString();
		User = in.readParcelable(null);
		
		in.readList(tags, null);
		Timestamp = in.readParcelable(null);
		replies = in.readInt();
		Video = in.readString();
	}
}