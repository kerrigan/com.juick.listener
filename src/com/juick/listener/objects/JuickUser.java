package com.juick.listener.objects;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;


public class JuickUser implements Parcelable {

    public int UID = 0;
    public String UName = null;
    public Object Avatar = null;
    public String FullName = null;

    public static JuickUser parseJSON(JSONObject json) throws JSONException {
        JuickUser juser = new JuickUser();
        juser.UID = json.getInt("uid");
        juser.UName = json.getString("uname");
        if (json.has("fullname")) {
            juser.FullName = json.getString("fullname");
        }
        return juser;
    }

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(UID);
		dest.writeString(UName);
		dest.writeString(FullName);
	}
	
	
	public JuickUser(){}
	
	public JuickUser(Parcel in){
		UID = in.readInt();
		UName = in.readString();
		FullName = in.readString();
	}
	
	
	public static final Parcelable.Creator<JuickUser> CREATOR = new Parcelable.Creator<JuickUser>() {
		public JuickUser createFromParcel(Parcel in) {
			return new JuickUser(in);
		}

		public JuickUser[] newArray(int size) {
			return new JuickUser[size];
		}
	};
}
