package com.juick.listener.services;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.juick.listener.R;
import com.juick.listener.objects.JuickMessage;
import com.juick.listener.ws.WsClient;
import com.juick.listener.ws.WsClientListener;

public class WSService extends Service {

	public static final int MSG_REGISTER_CLIENT = -2;
	public static final int MSG_UNREGISTER_CLIENT = -1;
	public static final int ACTIVITY_CONNECTED = 0;
	public static final int NEW_POST_OBTAINED = 1;
	public static final int NEW_COMMENT_OBTAINED = 2;
	private NotificationManager nManager;
	private int NOTIFICATION = R.string.local_service_started;
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	
	
	public class LocalBinder extends Binder {
		public WSService getService(){
			return WSService.this;
		}
	}
	
	@Override
	public void onCreate() {
		nManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		new Thread(new PostListenerRunnable()).start();
		new Thread(new CommentsListenerRunnable()).start();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(WSService.class.getName(), "Received start id " + startId + ": " + intent);
		// TODO Auto-generated method stub

		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		nManager.cancel(NOTIFICATION);
	}
	
	final Messenger mMessenger = new Messenger(new IncomingHandler(this));
	public Messenger mClient;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mMessenger.getBinder();
	}
	
	/*
	 * Websocket listeners
	 */
	
	
	private static class IncomingHandler extends Handler {
		
		private WeakReference<WSService> mService;

		public IncomingHandler(WSService service) {
			mService = new WeakReference<WSService>(service);
		}
		
		@Override
		public void handleMessage(Message msg) {
			Message clientMsg;
			if(mService == null)
				return;
			Messenger newClient;
			Bundle bundle;
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				newClient = msg.replyTo;
				mService.get().mClients.add(newClient);
				//TODO: здесь рассылаем последние сообщения
				break;
			case MSG_UNREGISTER_CLIENT:
				mService.get().mClients.remove(msg.replyTo);
				break;
			case NEW_POST_OBTAINED:
				//TODO: послать сообщение
				JuickMessage mNewPost =  (JuickMessage)((Bundle)msg.obj).get("message");
				bundle = new Bundle();
				bundle.putParcelable("message", mNewPost);
				clientMsg = Message.obtain(null, NEW_POST_OBTAINED, bundle);
				mService.get().broadcast(clientMsg);
				break;
			case NEW_COMMENT_OBTAINED:
				//TODO: послать сообщение
				JuickMessage mNewComment = (JuickMessage)((Bundle)msg.obj).get("message");
				bundle = new Bundle();
				bundle.putParcelable("message", mNewComment);
				clientMsg = Message.obtain(null, NEW_COMMENT_OBTAINED, bundle);
				mService.get().broadcast(clientMsg);
				break;
			default:
				super.handleMessage(msg);
				break;
			}
		}
	}
	
	private void broadcast(Message msg){
		for (int i = mClients.size()-1;i >= 0; --i) {
			try{
				mClients.get(i).send(msg);	
			} catch (RemoteException e) {
				mClients.remove(i);
				Log.e("WSService", "Connection is dead");
			}
		}
	}
	
	
	private class PostListenerRunnable implements Runnable {

		private WsClient mWSClient;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			mWSClient = new WsClient();
			mWSClient.setListener(new WsClientListener() {
				
				@Override
				public void onWebSocketTextFrame(String data) throws IOException {
					try {
						JuickMessage newPost = JuickMessage.parseJSON(new JSONObject(data));
						Bundle bundle = new Bundle();
						bundle.putParcelable("message", newPost);
						mMessenger.send(Message.obtain(null, NEW_POST_OBTAINED, bundle));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						Log.e("WSService", "Service is dead");
					}
				}
			});
			mWSClient.connect("api.juick.com", 8080, "/all", null);
			mWSClient.readLoop();
		}
		
	}
	
	private class CommentsListenerRunnable implements Runnable {

		private WsClient mWSClient;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			mWSClient = new WsClient();
			mWSClient.setListener(new WsClientListener() {
				
				@Override
				public void onWebSocketTextFrame(String data) throws IOException {
					try {
						JuickMessage newPost = JuickMessage.parseJSON(new JSONObject(data));
						Bundle bundle = new Bundle();
						bundle.putParcelable("message", newPost);
						mMessenger.send(Message.obtain(null, NEW_COMMENT_OBTAINED, bundle));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						Log.e("WSService", "Service is dead");
					}
				}
			});
			mWSClient.connect("api.juick.com", 8080, "/replies", null);
			mWSClient.readLoop();
		}
	}
	
	/*
	 * Client methods
	 */
	
	public List<JuickMessage> getLastComments(){
		return new ArrayList<JuickMessage>();
	}
	
	public List<JuickMessage> getLastPosts(){
		return new ArrayList<JuickMessage>();
	}
}
