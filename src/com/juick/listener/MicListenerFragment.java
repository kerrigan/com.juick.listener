package com.juick.listener;

import ca.uol.aig.fftpack.RealDoubleFFT;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class MicListenerFragment extends Fragment {
	private boolean started = false;
	private RealDoubleFFT transformer;
	private int blockSize = 256;
	int frequency = 8000;
	int channelConfig = AudioFormat.CHANNEL_CONFIGURATION_MONO;
	int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
	

	private RecordAudio recordTask;

	
	
	
	private ImageView spectrumImg;
	private Bitmap bitmap;
	private Canvas canvas;
	private Paint paint;
	private Button toggleBtn;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		transformer = new RealDoubleFFT(blockSize);
		
		View view = inflater.inflate(R.layout.mic_layout, null);
	
		toggleBtn = (Button) view.findViewById(R.id.btnStartRecord);
		spectrumImg = (ImageView)view.findViewById(R.id.imgSpectrum);
		bitmap = Bitmap.createBitmap((int)256, (int)100, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		
		paint = new Paint();
		paint.setColor(Color.GREEN);
		spectrumImg.setImageBitmap(bitmap);
		
		toggleBtn.setOnClickListener(new ToggleBtnClickListener());
		return view;
	}
	
	
	private class ToggleBtnClickListener implements OnClickListener {

	
		@Override
		public void onClick(View v) {
			if(!started){
				started = true;
				toggleBtn.setText("Stop");
				recordTask = new RecordAudio();
				recordTask.execute();
			} else {
				started = false;
				toggleBtn.setText("Record");
				recordTask.cancel(true);
			}
		}
		
	}
	
	private class RecordAudio extends AsyncTask<Void, double[], Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try{
				int bufferSize = AudioRecord.getMinBufferSize(frequency, channelConfig, audioEncoding);
				AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
						frequency, channelConfig, audioEncoding, bufferSize);

				short[] buffer = new short[blockSize];
				double[] toTransform = new double[blockSize];
				
				
				audioRecord.startRecording();
				while(started){
					int bufferReadResult = audioRecord.read(buffer, 0, blockSize);
					for (int i = 0; i < blockSize && i < bufferReadResult; ++i) {
						toTransform[i] = (double)buffer[i] / Short.MAX_VALUE;
					}
					transformer.ft(toTransform);
					publishProgress(toTransform);
				}
				
				audioRecord.stop();
			}catch(Throwable t){
				Log.e(this.getClass().getName(), "Recording failed");
			}
			return null;
		}
		
		
		@Override
		protected void onProgressUpdate(double[]... toTransform) {
			// TODO Auto-generated method stub
			canvas.drawColor(Color.BLACK);
			
			int upy = 100;
			int prevX = 0;
			int prevDownY = (int)(100 - (toTransform[0][0] * 10));
			
			for (int i = 1; i < toTransform[0].length; i++) {
				int x = i;
				int downy = (int)(100 - (toTransform[0][i] * 10));
				
				canvas.drawLine(prevX, prevDownY, x, downy, paint);
				prevX = x;
				prevDownY = downy;
			}
			spectrumImg.invalidate();
		}
		
	}
}
